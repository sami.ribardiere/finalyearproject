from django.test import TestCase, Client
from django.urls import reverse
from main.models import *
import json
from rest_framework import status


from datetime import date

from main.serializers import MealSerializer, PreferencesSerializer

class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.meal_url = reverse('meal')
        self.openai_url = reverse('openai_populate')
        self.register_url = reverse('register')
        self.login_url = reverse('login')
        self.profile_url = reverse('profile')
        self.reco_url = reverse('reco')
        self.onemeal_url = reverse('reco')
        self.physic_url = reverse('physic')
        self.email_url = reverse('email')
        self.chart_url = reverse('chart')
        self.meal_url = reverse('onemeal')
        self.mealquery_url = reverse('mealquery')

        self.user = User.objects.create(
            username="toto",
            email="sam.rib@g.com",
            password="333333xxx",
            age=30,
        )

        self.physic = Physic.objects.create(
            user=self.user, height=[170, 172], weight=[65, 63], 
            height_timestamp=[date(2022, 2, 1), date(2022, 1, 1)], 
            weight_timestamp=[date(2022, 2, 1), date(2022, 1, 1)], 
            weight_goal=60, duration=90)
    
        self.meal = Meal.objects.create(
            name='test meal',
            recipe_id=123,
            rating=4,
            nutri_values=[1.00, 2.00, 3.00],
            foodpic='testpic.jpg',
            ingredients=['ingredient1', 'ingredient2'],
            description='test description',
            steps=['step1', 'step2'],
            n_steps=2,
            tags=['tag1', 'tag2']
        )
       
        


    def test_TC1_register_new_user_with_valid_email(self):
        response = self.client.post(self.register_url, {
            'email': 's@gmail.com',
            'username': 'Sami',
            'password': 'BobyBernard3+',
            'age': 21,
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('success', str(response.content))

    def test_TC2_register_new_user_with_invalid_email(self):
        response = self.client.post(self.register_url, {
            'email': 's@@@gmailcom',
            'username': 'Sami',
            'password': 'BobyBernard3+',
            'age': 21,
        })
        self.assertEqual(response.status_code, 200)

    def test_TC4_login_with_valid_credentials(self):
        # Register the user first
        self.client.post(self.register_url, {
            'email': 's@gmail.com',
            'username': 'Sami',
            'password': 'BobyBernard3+',
            'age': 21,
        })

        # Then attempt to login with valid credentials
        response = self.client.post(self.login_url, {
            'email': 's@gmail.com',
            'password': 'BobyBernard3+',
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('go to rating', str(response.content))

    def test_TC5_login_with_invalid_credentials(self):
        # Register the user first
        self.client.post(self.register_url, {
            'email': 's@gmail.com',
            'username': 'Sami',
            'password': 'BobyBernard3+',
            'age': 21,
        })

        # Then attempt to login with invalid credentials
        response = self.client.post(self.login_url, {
            'email': 's@gmail.com',
            'password': 'wrongpassword',
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('Incorrect password', str(response.content))


        
   


    def test_TC6_invalid_login_with_incorrect_password(self):
        response = self.client.post(reverse('login'), {
            'email': 'testuser@gmail.com',
            'password': 'WrongPassword'
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('User not found !', str(response.content))

    

    def test_TC7_update_user_profile(self):
        user = User.objects.create(email='s@gmail.com', username='Sami', age=21,password = '333333xx')
        data = {
            'userid': user.id,
            'username': 'New Name',
            'profilepic': 'newpic.jpg',
            'email': 'newemail@gmail.com',
        }
        response = self.client.put(self.profile_url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('your account has been updated', str(response.content))

    def test_TC8_add_workout_for_user(self):
        # create user
        response = self.client.post(self.register_url, {
            'email': 's@ggmail.com',
            'username': 'Sami',
            'password': 'BobyBernard3+',
            'age': 21,
        })
        user_id = response.json()['id']
        
        # create physical profile for the user
        data = {
            'user_id': user_id,
            'height': 175,
            'weight': 70,
            'weight_goal': 65,
            'duration': 60,
            'goal': 1,
        }
        response = self.client.put(self.physic_url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Your update has been saved', str(response.content))

    def test_TC9_get_user_profile(self):
        user = User.objects.create(email='s@gmail.com', username='Sami', age=21)
        response = self.client.post(self.profile_url, {'user_id': user.id})
        self.assertEqual(response.status_code,200)

    def test_TC11_login_with_nonexistent_user(self):
        response = self.client.post(self.login_url, {
            'email': 'nonexistent@gmail.com',
            'password': 'testpassword'
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('User not found', str(response.content))

    def test_TC12_email_verification_with_correct_code(self):
        user = User.objects.create(email='testuser@gmail.com', username='testuser', password='testpassword', age=25)
        token = SignUpToken.objects.create(user=user, code=123456)
        response = self.client.post(self.email_url + '?token=' + str(token.id), {'userid': user.id, 'code': 123456})
        self.assertEqual(response.status_code, 200)
        self.assertIn('Email Verified Successfully', str(response.content))


    def test_TC13_email_verification_with_incorrect_code(self):
        user = User.objects.create(email='testuser@gmail.com', username='testuser', password='testpassword', age=25)
        token = SignUpToken.objects.create(user=user, code=123456)
        response = self.client.post(self.email_url + '?token=' + str(token.id), {'userid': user.id,'code': 654321})
        self.assertEqual(response.status_code, 200)
        self.assertIn('Wrong code', str(response.content))



    # ... (other test cases)


    

    

    def test_getinfochart(self):
        data = {'user_id': self.user.id}
        response = self.client.post(self.chart_url, data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['physic_height'], [170, 172])
        self.assertEqual(response.data['physic_weight'], [65, 63])
        self.assertEqual(response.data['height_days'], [(date.today() - date(2022, 2, 1)).days, (date.today() - date(2022, 1, 1)).days])
        self.assertEqual(response.data['weight_days'], [(date.today() - date(2022, 2, 1)).days, (date.today() - date(2022, 1, 1)).days])
        self.assertEqual(response.data['weight_goal'], 60)
        self.assertEqual(response.data['duration'], 90)
        self.assertEqual(response.data['age'], 30)

        
    def test_getinfochart_invalid_user_id(self):
        # create a new user

        data = {'user_id': 1000}  # use non-existent id
        response = self.client.post(self.chart_url, data, format='json')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data, {'detail': 'Not found.'})

        # clean up
       
    def test_TC15_post_meal(self):
        # Test creating a new meal
        url = reverse('meal')
        data = {
            "name": "New Meal",
            "recipe_id": 2,
            "description": "New meal description",
            "foodpic": "https://example.com/new_meal.jpg",
            "nutri_values": [100.0, 50.0, 20.0],
            "ingredients" : ['ingredient1', 'ingredient2'],
            "steps" : ['step1', 'step2'],
            "tags" : ['tag1', 'tag2'],
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_TC16_get_meals(self):
        # Test getting meals
        url = reverse('meal')
        response = self.client.get(url, format='json')
        meals = Meal.objects.all()
        serializer = MealSerializer(meals, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['msg'], serializer.data)

    def test_TC17_put_user_pref(self):
    # Test updating user preferences
        url = reverse('pref_info')

        response = self.client.post(self.register_url, {
            'email': 's@ggmail.com',
            'username': 'Sami',
            'password': 'BobyBernard3+',
            'age': 21,
        })

        user_id = response.json()['id']
        user = User.objects.get(pk=user_id)
        p = Preferences.objects.create(
            user=user,
            fav_ingredients="Onions",
            fav_crountry_meal="Mexican",
            budget=75,
            fav_complexity=1,
            fav_meals="Pizza"  # Add the missing fav_meals field
        )
        p.save()

        data = {
            "user": user_id,
            "fav_ingredients": "Onions",
            "fav_crountry_meal": "Mexican",
            "budget": 75,
            "fav_complexity": 1,
            "fav_meals": "Pizza"
        }
        response = self.client.put(url, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        pref = Preferences.objects.get(user=user.id)
        serializer = PreferencesSerializer(pref)
        self.assertEqual(response.data, serializer.data)

    def test_TC18_get_user_pref(self):
            # Test getting user preferences
            url = reverse('pref_info')
            data = {"user_id": self.user.id}
            response = self.client.get(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            pref = Preferences.objects.get(user=self.user.id)
            serializer = PreferencesSerializer(pref)
            self.assertEqual(response.data, serializer.data)

    def test_TC20_get_user_pref(self):
        url = reverse('pref_info')
        response = self.client.post(self.register_url, {
            'email': 'testuser@example.com',
            'username': 'TestUser',
            'password': 'testpassword123',
            'age': 25,
        })

        user_id = response.json()['id']
        user = User.objects.get(pk=user_id)
        pref = Preferences.objects.create(
            user=user,
            fav_ingredients='Tomatoes',
            fav_crountry_meal='Italian',
            budget=100,
            fav_complexity=2,
            fav_meals='Pasta'
        )
        pref.save()

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer = PreferencesSerializer(pref)
        self.assertEqual(response.data, serializer.data)

    # TC22: Test rating a meal using RateMealView
    def test_TC22_rate_meal(self):
        user = User.objects.create(email='testuser@example.com', username='TestUser', password='testpassword123', age=25)
        user.save()

        meal = Meal.objects.create(name='Test Meal', recipe_id=1, description='Test meal description', foodpic='https://example.com/test_meal.jpg')
        meal.save()

        url = reverse('rate')
        data = {
            'user_id': user.id,
            'meal_id': meal.id,
            'rating': 4,
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        rating = ratings.objects.get(user=user, meal=meal)
        self.assertEqual(rating.rating, 4)

    # TC23: Test first rating a meal using firstRatingView
    def test_TC23_first_rating_meal(self):
        user = User.objects.create(email='testuser@example.com', username='TestUser', password='testpassword123', age=25)
        user.save()

        meal = Meal.objects.create(name='Test Meal', recipe_id=1, description='Test meal description', foodpic='https://example.com/test_meal.jpg')
        meal.save()

        url = reverse('firstrating')
        data = {
            'user_id': user.id,
            'meal_id': meal.id,
            'rating': 4,
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        rating = ratings.objects.get(user=user, meal=meal)
        self.assertEqual(rating.rating, 4)
        self.assertTrue(user.isnexuser)

    # TC24: Test posting a workout using Workout view
    def test_TC24_post_workout(self):
        user = User.objects.create(email='testuser@example.com', username='TestUser', password='testpassword123', age=25)
        user.save()

        workout_obj = workout.objects.create(user=user)
        workout_obj.workout_list = [["workout 1", "workout 2"], ["workout 3", "workout 4"]]
        workout_obj.workout_names = ["workout name 1", "workout name 2"]
        workout_obj.save()

        url = reverse('workout')
        data = {
            'user_id': user.id,
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['workout_list'], workout_obj.workout_list)
        self.assertEqual(response.data['workout_names'], workout_obj.workout_names)


    def test_recomended_meal(self):
        data = {"user_id": self.user.id}
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue("msg" in response.data)

        meals = response.data["msg"]
        self.assertTrue(len(meals) > 0)
        for meal in meals:
            self.assertTrue(meal["name"] in self.user.recomeal)

    def test_openai_meal(self):
        data = {"user_prompt": "I prefer low-carb meals."}
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue("name" in response.data)
        self.assertTrue("image_url" in response.data)
        self.assertTrue("ingredients" in response.data)
        self.assertTrue("description" in response.data)
        self.assertTrue("nutri_values" in response.data)

        self.assertTrue(len(response.data["ingredients"]) > 0)
        self.assertTrue(len(response.data["nutri_values"]) == 4)

    def test_openai_sport(self):
        data = {"user_id": self.user.id, "nbr_workout": 5}
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue("list_workout" in response.data)
        self.assertTrue("list_name" in response.data)

        list_workout = response.data["list_workout"]
        list_name = response.data["list_name"]

        self.assertTrue(len(list_workout) > 0)
        self.assertTrue(len(list_name) > 0)
