import threading
from time import sleep
import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors



from .models import *




class testThread(threading.Thread):

    def __init__(self,user_id):
        self.user_id = user_id
        super(testThread, self).__init__()

    


    
    def run(self):
        def get_similar_food(food_name,user_ratings):

            item_similarity_df = pd.read_csv('corr.csv',index_col=0)
            
            

            similar_score = item_similarity_df[food_name]*(user_ratings-2.5)
            
            similar_score = similar_score.sort_values(ascending=False)
            return similar_score

        try:
            rating_list = ratings.objects.filter(user=self.user_id)
            meal_ids = [rating.meal.id for rating in rating_list]
            meal_ratings = rating_list.values('rating').reverse()
            meal_name = Meal.objects.filter(pk__in=meal_ids).values('name')
            print(meal_name)

            name_list = [item["name"] for item in meal_name]
            ratings_list = [item["rating"] for item in meal_ratings]

                

            list_food_reco = list(zip(name_list,ratings_list))
                


                

            similar_food = pd.DataFrame()
            

            for name,rating in list_food_reco:
                similar_food = similar_food.append(get_similar_food(name,rating),ignore_index=True)
            
            df1 = similar_food.sum().sort_values(ascending=False)
            print('test')
            r = User.objects.filter(pk=self.user_id).update(recomeal = df1.index.tolist())
            print('done')
           

        except Exception as e:
            print(e)
        

class testThread_KNN(threading.Thread):

    def __init__(self, user_id, k=5):
        self.user_id = user_id
        self.k = k
        super(testThread_KNN, self).__init__()

    def run(self):
        def get_similar_food(food_name, user_ratings, k):

            item_similarity_df = pd.read_csv('corr.csv', index_col=0)
            item_similarity_df = item_similarity_df.round(5)

            if item_similarity_df.isna().any().any():
                print("NaN values found in item_similarity_df")
                print(item_similarity_df)
                item_similarity_df = item_similarity_df.fillna(0)

            knn_model = NearestNeighbors(metric='cosine', algorithm='brute')
            knn_model.fit(item_similarity_df.values)

            food_vector = item_similarity_df.loc[food_name].values.reshape(1, -1)
            if np.isnan(food_vector).any():
                print("NaN values found in food_vector")
                print(food_vector)
                food_vector[np.isnan(food_vector)] = 0

            distances, indices = knn_model.kneighbors(food_vector, n_neighbors=k + 1)

            similar_score = item_similarity_df.iloc[indices.flatten()].copy()
            similar_score = similar_score.apply(lambda x: x * (user_ratings - 2.5))
            similar_score = similar_score.sum(axis=0).sort_values(ascending=False)

            return similar_score

        try:
            rating_list = ratings.objects.filter(user=self.user_id)
            meal_ids = [rating.meal.id for rating in rating_list]
            meal_ratings = rating_list.values('rating').reverse()
            meal_name = Meal.objects.filter(pk__in=meal_ids).values('name')
            print(meal_name)

            name_list = [item["name"] for item in meal_name]
            ratings_list = [item["rating"] for item in meal_ratings]

            list_food_reco = list(zip(name_list, ratings_list))

            similar_food = pd.DataFrame()

            for name, rating in list_food_reco:
                similar_food = pd.concat([similar_food, get_similar_food(name, rating, self.k)], axis=1)

            df1 = similar_food.sum().sort_values(ascending=False)
            print('test')
            r = User.objects.filter(pk=self.user_id).update(recomeal=df1.index.tolist())
            print('done')

        except Exception as e:
            print(e)